#include <stdio.h>

void swap(int &a, int &b)
{
    int temp = a;
    a = b;
    b = temp;
}

void print_heap(int *array, int size) {
    for(int i = 0; i < size; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

void heapify(int *array, int size, int root) 
{
    int child = root * 2 + 1;
    if(child < size - 1 && array[child] < array[child+1]) child++;

    if(array[root] < array[child]) {
        // printf("root:%d, child:%d, array[root]:%d, array[child]:%d\n", root, child, array[root], array[child]);
        swap(array[root], array[child]);
        if(child <= (size - 1) / 2) heapify(array, size, child);
    }
}

void heap_sort(int *array, int last_index)
{
    swap(array[0], array[last_index]);
    int root = 0;
    int child = 1;

    while((child - 1) / 2 < last_index) {
        child = 2 * root + 1;
        if(child < last_index - 1 && array[child] < array[child+1]) child++;

        if(child < last_index && array[root] < array[child])
            swap(array[root], array[child]);

        root = child;
    }
}

int main(void)
{
    int heap[1000];
    int size;
    scanf("%d", &size);

    for(int i = 0 ; i < size ; i++) {
        scanf("%d", &heap[i]);
    }

    print_heap(heap, size);
    for(int i = (size-1)/2 ; i >= 0; i--) {
        heapify(heap, size, i);
    }

    for(int i = size - 1; i >= 0 ; i--) {
        heap_sort(heap, i);           
        // print_heap(heap, size);
    }
    print_heap(heap, size);

    return 0;
}