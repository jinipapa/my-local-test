#include <iostream>

int count_table[26][200001];
char buffer[200001];

int main(void)
{
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int count = 0;

    std::cin >> buffer;
    // std::cout << buffer << '\n';

    count_table[buffer[0] - 'a'][0] = 1;
    for(int i = 1; buffer[i] != '\0';i++) {
        int index = buffer[i] - 'a';
        for(int j = 0; j < 26 ; j++) {
            count_table[j][i] = count_table[j][i-1];
        }
        count_table[buffer[i]-'a'][i]++;
    }

    int T;
    std::cin >> T;

    for(int tc = 1; tc <= T; tc++) {
        char ch;
        int start, end;
        std::cin >> ch;
        std::cin >> start >> end;
        
        int result = 0;
        int index = ch - 'a';

        // std::cout << ch << ' ' << start << ' ' << end << ' ' << '\n';

        if(start == 0) {
            result = count_table[index][end];
        } else {
            result = count_table[index][end] - count_table[index][start - 1];
        }
        std::cout << result << '\n';
    }

    return 0;
}
