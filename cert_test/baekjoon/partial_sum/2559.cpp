#include <stdio.h>

int sum_table[100001];
int num[100001];

int main(void)
{
    int N, K;
    scanf("%d %d", &N, &K);

    int max = -1000000000;
    sum_table[0] = 0;
    for(int i = 1 ; i <= N; i++) {
        scanf("%d", &num[i]);

        if(i <= K) {
            sum_table[i] = sum_table[i-1] + num[i];  
        } else {
            sum_table[i] = sum_table[i-1] - num[i-K] + num[i];
        }

        if(i >= K) {
            if(max < sum_table[i]) max = sum_table[i];
        }
    }

    printf("%d\n", max);

    return 0;
}