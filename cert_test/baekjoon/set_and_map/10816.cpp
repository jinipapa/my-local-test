#include <stdio.h>


void merge(int *table, int begin, int middle, int end)
{
    int i = begin;
    int j = middle + 1;
    int k = 0;
    int size = end - begin + 1;

    int *sorted = new int[size];

    while(i <= middle && j <= end) {
        if(table[i] <= table[j]) sorted[k++] = table[i++];
        else sorted[k++] = table[j++];
    }

    if( i > middle) {
        for(int t = j; t <= end; t++) sorted[k++] = table[t];
    } else {
        for(int t = i; t <= middle; t++) sorted[k++] = table[t];
    }

    for(int t = 0; t < size; t++) {
        table[begin + t] = sorted[t];
    }

    delete[] sorted;
}

void merge_sort(int *table, int begin, int end)
{
    if(begin < end) {
        int middle = (begin + end) / 2;
        merge_sort(table, begin, middle);
        merge_sort(table, middle + 1, end);
        merge(table, begin, middle, end);
    }
}

int table_n[500001];
// int table_m[500001];

bool find_range(int table[], int size, int num, int& sidx, int &eidx)
{
    int begin = 0;
    int end = size -1;
    int middle = (begin + end) / 2;
    bool found = false;

    while(begin <= end) {
        if(table[middle] == num) {
            found = true;
            break;
        }
        if(table[middle] < num) {
            begin = middle + 1;
        } else {
            end = middle - 1;
        }
        middle = (begin + end) / 2;
    }

    if(!found) {
        // printf("%d not found", num);
        return false;
    }

    sidx = middle - 1;
    while(sidx >= 0) {
        if(num != table[sidx]) {
            break;
        }
        sidx--;
    }
    sidx++;

    eidx = middle + 1;
    while(eidx <= size - 1) {
        if(num != table[eidx]) {
            break;
        }
        eidx++;
    }
    eidx--;

    // printf("%d:%d[%d]\n", sidx, eidx, middle);

    return true;
}

void print_table(int table[], int N) {
    for(int i = 0; i < N ; i ++) {
        printf("%d ", table[i]);
    }
    printf("\n");
}

#if 0
int main(void)
{
    int N;
    
    scanf("%d", &N);
    for(int i = 0 ; i < N;  i++) {
        scanf("%d", &table_n[i]);
    }

    merge_sort(table_n, 0, N - 1);
    // print_table(table_n, N);

    int M;
    scanf("%d", &M);
    for(int i = 0 ; i < M ; i++) {
        int num;
        int sidx = 0;
        int eidx = 0;

        scanf("%d", &num);

        if(find_range(table_n, N, num, sidx, eidx) == false) {
            printf("0 ");            
        } else {
            printf("%d ", eidx - sidx + 1);
        }
    }
    printf("\n");
    
    return 0;
}
#else 

typedef struct _NumCount {
    int num;
    int count;
    
    _NumCount()
     : num(0), count(0) 
    {}
} NumCount;

NumCount num_count[500001];

int get_count(NumCount table[], int size, int num) {
    int begin = 0;
    int end = size - 1;
    int middle = (begin + end) / 2;

    while(begin <= end) {
        if(table[middle].num == num) {
            return table[middle].count;
        } else if(table[middle].num < num) {
            begin = middle + 1;
        } else {
            end = middle - 1;
        }
        middle = (begin + end) / 2;
    }

    return 0;
}

int main(void)
{
    int N;
    
    scanf("%d", &N);
    for(int i = 0 ; i < N;  i++) {
        scanf("%d", &table_n[i]);
    }

    merge_sort(table_n, 0, N - 1);
    
    num_count[0].num = table_n[0];
    num_count[0].count = 1;

    int idx = 0;
    for(int i = 1; i < N; i++) {
        if(num_count[idx].num == table_n[i]) {
            num_count[idx].count++;
        } else {
            ++idx;
            num_count[idx].num = table_n[i];
            num_count[idx].count = 1;
        }
    }
    
    int M;
    scanf("%d", &M);
    for(int i = 0 ; i < M ; i++) {
        int num;
        scanf("%d", &num);
        printf("%d ", get_count(num_count, idx + 1, num));
    }
    printf("\n");
    
    return 0;
}
#endif