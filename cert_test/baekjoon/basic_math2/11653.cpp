#include <stdio.h>


int main(void)
{
    unsigned int M, N;
    scanf("%u %u", &M, &N);

    bool table[N+1];
    table[0] = false;
    table[1] = false;
    for(unsigned int i = 2; i<=N; i++) {
        table[i] = true;
    }

    for(unsigned int i = 2; i*i <= N; i++) {
        if(table[i] == false) continue;

        for(unsigned int j = i*i; j <= N; j +=i) {
            table[j] = false;
        }
    }

    for(unsigned int i = M ; i <=N ; i++) {
        if(table[i] == true) printf("%d\n", i);
    }
    
    return 0;
}