#include <stdio.h>

int main(void) 
{
    unsigned int num[100000];
    unsigned int count = 0;
    unsigned int max = 0;
    while(1)
    {
        unsigned n;
        scanf("%d", &n);
        if(n == 0) break;
        num[count++] = n;

        if(max < n) max = n;
    }

    // 에라토스테네스의 채
    bool table[2*max+1];
    table[0] = table[1] = false;
    for(unsigned int i = 2; i <= 2*max; i++) {
        table[i] = true;
    }

    for(unsigned int i = 2; i <= 2*max; i++) {
        if(table[i] == false) continue;
        for(unsigned int j = i+i; j <= 2*max; j += i) {
            table[j] = false;
        }
    }

    
    for(unsigned int i = 0; i < count; i++) {
        unsigned int res = 0;
        for(unsigned int j = num[i] + 1; j <= 2*num[i] ; j++) {
            if(table[j] == true) res++;
        }
        printf("%d\n", res);
    }    

    return 0;
}