#include <stdio.h>

int main(void) 
{
    unsigned int N;
    scanf("%d", &N);

    unsigned int res = N / 3;
    for(unsigned int i = 0; i <= res ; i++) {
        int temp = N - 3*i;
        if(temp % 5 == 0) {
            printf("%d\n", i+(temp/5));
            return 0;
        }
    }

    printf("-1\n");

    return 0;
}