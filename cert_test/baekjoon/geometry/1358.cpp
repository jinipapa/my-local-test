#include <stdio.h>

int main(void)
{
    int W, H, X, Y, P;
    scanf("%d %d %d %d %d", &W, &H, &X, &Y, &P);
    int R = H/2;
    int count = 0;
    for(int i = 0 ; i < P ; i++) {
        int x, y;
        scanf("%d %d", &x, &y);

        if(x < X - R || x > X + W + R) continue;
        if(y < Y || y > Y + H) continue;

        if( x >= X && x <= X + W) {
            count++;
        } else if(x >= X - R && x < X) {
            if(R*R >= (X-x)*(X-x) + (Y+R-y)*(Y+R-y)) count++;
        } else {
            if(R*R >= (X+W -x)*(X+W -x) + (Y+R-y)*(Y+R-y)) count++;
        }
    }

    printf("%d\n", count);

    return 0;
}