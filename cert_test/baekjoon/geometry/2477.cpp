#include <stdio.h>

int main(void)
{
    int K;
    scanf("%d", &K);
    
    int dir[6] = {0, };
    int dist[6] = {0, };

    int long_width = 0;
    int long_width_dir = 0;
    int long_height = 0;
    int long_height_dir = 0;
    for(int i = 0; i < 6 ; i++) {
        scanf("%d %d", &dir[i], &dist[i]);
        if(dir[i] == 1 || dir[i] == 2 ) {
            if(long_width < dist[i]) {
                long_width = dist[i];
            }
        } else {
            if(long_height < dist[i]) {
                long_height = dist[i];
            }
        }
    }

    int first_long_index = 0;
    for(int i = 0 ; i < 6; i++) {
        if(dist[i] == long_width || dist[i] == long_height) {
            first_long_index = i;
            break;
        }
    }

    if(first_long_index == 0 && (dist[5] == long_width || dist[5] == long_height)) {
        first_long_index = 5;
    }

    // printf("logw:%d, longh:%d\n");

    int area = long_width*long_height - dist[(first_long_index+3)%6]*dist[(first_long_index+4)%6];

    printf("%d\n", area*K);

    return 0;
}