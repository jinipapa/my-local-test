#include <iostream>

using namespace std;

#define Q_MAX_SIZE 1000001

class Queue {
public:
    void Push(int item) {
        items[rear++] = item;
    }

    bool Empty() {
        if(front == rear) return true;
        return false;
    }

    unsigned int Size() {
        return rear - front;
    }

    int Pop() {
        return items[front++];
    }

    int Front() {
        return items[front];
    }

    int Back() {
        return items[rear-1];
    }

private:
    int items[Q_MAX_SIZE];
    int front = 0;
    int rear = 0;
};

int main(void)
{
    int N;
    cin >> N;
    Queue num_q;
    for(unsigned int  i = 1; i <= N; i++) {
        num_q.Push(i);
    }

    while(num_q.Size() != 1) {
        int trash = num_q.Pop();
        if(num_q.Size() == 1) break;

        int fr_num = num_q.Pop();
        num_q.Push(fr_num);
    }

    cout << num_q.Pop() << '\n';

    return 0;
}