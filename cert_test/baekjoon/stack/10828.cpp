#include <iostream>
#include <string>

int stack[100000];
int pos = -1;

int size()
{
    return pos + 1;
}

void push(int num) {
    stack[++pos] = num;
}

int empty() {
    if(pos == -1) return 1;

    return 0;
}

int pop() {
    if(empty() == 1) return -1;

    int val = stack[pos];
    pos--;
    return val;
}

int top() {
    if(empty() == 1) return -1;

    return stack[pos];
}

int main(void)
{
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int N;
    std::cin >> N;

    for(int i = 0; i < N; i++) {
        std::string command;
        std::cin >> command;
        if(command == "push") {
            int num;
            std::cin >> num;
            push(num);
        } else if(command == "pop") {
            std::cout << pop() << "\n";
        } else if(command == "empty") {
            std::cout << empty() << "\n";
        } else if(command == "top") {
            std::cout << top() << "\n";
        } else if(command == "size") {
            std::cout << size() << "\n";
        }
    }

    return false;
}  