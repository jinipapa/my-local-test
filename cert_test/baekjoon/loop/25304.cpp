#include <iostream>

using namespace std;

int main(void)
{
    ios::sync_with_stdio(false);
    cin.tie(nullptr);

    unsigned int X;
    cin >> X;
    unsigned int N;
    cin >> N;
    unsigned int sum = 0;
    for(unsigned int i =0; i < N; i++) {
        unsigned int a, b;
        cin >> a >> b;
        sum += a*b;
    }
    
    if(sum == X) {
        cout << "Yes\n";
    } else {
        cout << "No\n";
    }

    return 0;
}