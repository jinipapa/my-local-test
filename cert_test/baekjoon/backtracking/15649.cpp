#include <stdio.h>

#define MAX 9

int table[MAX] = { 0, };
bool visit[MAX] = { false, };
int n, m;

void make_list(int count)
{
	if (count == m) {
		for (int i = 0; i < m; i++) {
			printf("%d ", table[i]);
		}
		printf("\n");
		return;
	}

	for (int i = 0; i < n; i++) {
		if (visit[i] == false) {
			visit[i] = true;
			table[count] = i+1;
			make_list(count + 1);
			visit[i] = false;
		}
	}
}

int main(void)
{
	scanf("%d %d", &n, &m);
	make_list(0);

	return 0;
}