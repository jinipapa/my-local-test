#include <stdio.h>

#define MAX 16

int N;
int sum = 0;


bool table[MAX];


int absolute(int a, int b) {
    if(a > b) return a - b;
    else return b - a;
}

bool is_promising(int count){
    for(int i = 0 ; i < count ; i++) {
        if(table[count] == table[i] || count - i == absolute(table[count], table[i]) ) return false;
    }
    return true;
}

void find_solution(int count)
{
    if(count == N) {
        sum++;
        return;
    }
    
    for(int i = 0 ; i < N; i++) {
        table[count] = i;
        if(is_promising(count)) {
            find_solution(count + 1);
        }
    }   
}

int main(void)
{
    scanf("%d", &N);
    find_solution(0);
    printf("%d\n", sum);
    return 0;
}