#include <stdio.h>

#define MAX 9
int n, m;

int table[MAX];

void make_list(int count)
{
    if(count == m){
        for(int i = 0; i < m ; i++) {
            printf("%d ", table[i]);
        }
        printf("\n");
        return;
    }

    for(int i = 1; i <=n ; i++) {
        if (table[count-1] > i) continue; 
        table[count] = i;
        make_list(count + 1);
    }
}

int main(void)
{
    scanf("%d %d", &n, &m);
    table[0] = 0;
    make_list(0);
    return 0;
}