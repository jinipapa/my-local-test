#include <stdio.h>
#include <stdlib.h>
// Test Program

int table[21][21];
int n;
int min_diff = 1000000000;
bool selected[21] = {false,};

int get_diff()
{
	int a_team[11];
	int b_team[11];
	int a_count = 0;
	int b_count = 0;
	for(int i = 0 ; i < n; i++) {
		if(selected[i] == true) a_team[a_count++] = i;
		else b_team[b_count++] = i;
	}

	int a_score = 0;
	int b_score = 0;
	for(int i = 0 ; i < n / 2; i++) {
		for(int j = 0; j < n / 2; j++) {
			a_score += table[a_team[i]][a_team[j]];
			b_score += table[b_team[i]][b_team[j]];
		}
	}

	return abs(a_score - b_score);
}

void make_result(int idx, int count)
{
	if(count == n / 2) {
		int diff = get_diff();
		if(diff < min_diff) min_diff = diff;
		return;
	}

	for(int i = idx; i < n; i++) {
		if(selected[i] == true) continue;
		selected[i] = true;
		make_result(i, count + 1);
		selected[i] = false;
	}
}

int main(void)
{
	scanf("%d", &n);
	for(int i = 0 ; i < n; i++) {
		for(int j = 0; j < n; j++) {
			scanf("%d", &table[i][j]);
		}
	}

	make_result(0, 0);

	printf("%d\n", min_diff);
	return 0;
}
