#include <iostream>

using namespace std;

unsigned int count = 0;
unsigned int K;

void merge(int* A, int begin, int mid, int end)
{
    int i = begin;
    int j = mid+1;
    int size = end - begin + 1;
    int k = 0;

    int* temp = new int[size];

    while(i <= mid && j <= end) {
        if(A[i] <= A[j]) temp[k++] = A[i++];
        else temp[k++] = A[j++];
    }

    while(i <= mid) temp[k++] = A[i++];
    while(j <= end) temp[k++] = A[j++];

    for(unsigned int t = 0; t < size; t++) {
        if(++count == K) cout << temp[t] << '\n';
        A[begin+t] = temp[t];
    }

    delete [] temp;
}

void merge_sort(int* A, int begin, int end)
{
    if(begin < end) {
        int mid = (begin + end) / 2;
        merge_sort(A, begin, mid);
        merge_sort(A, mid+1, end);
        merge(A, begin, mid, end);
    }
}

int main(void)
{
    ios::sync_with_stdio(false);
    cin.tie(nullptr);

    unsigned int N;
    cin >> N >> K;

    int* A = new int[N];

    for(unsigned int i = 0; i < N; i++) {
        cin >> A[i];
    }

    merge_sort(A, 0, N - 1);
    if(count < K) cout << -1 << '\n';

    delete [] A;

    return 0;
}