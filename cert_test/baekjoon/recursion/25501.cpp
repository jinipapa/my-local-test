#include <iostream>
#include <string.h>

using namespace std;

int recursion(const char* s, int l, int r, int& count)
{
    count++;
    if(l >= r) return 1;
    else if(s[l] != s[r]) return 0;
    else return recursion(s, l+1, r-1, count);
}

int isPalindrome(const char* s, int& count)
{
    return recursion(s, 0, strlen(s) - 1, count);
}

int main(void)
{
    ios::sync_with_stdio(false);
    cin.tie(nullptr);

    int N;
    cin >> N;

    for(int i = 0; i < N; i++) {
        char *text = new char[2000];
        cin >> text;
        int count = 0;
        int res = isPalindrome(text, count);

        cout << res << ' ' << count << '\n';

        delete [] text;
    }

    return 0;
}