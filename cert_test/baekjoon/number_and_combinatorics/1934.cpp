#include <stdio.h>

int main(void)
{
    int tc;
    scanf("%d", &tc);

    for(int i = 0; i < tc; i++) {
        int A, B, temp;
        scanf("%d %d", &A, &B);

        if(A > B) {
            temp = A;
            A = B;
            B = temp;
        }

        int product = 1;
        int multi = 2;
        while(multi <= A) {
            if((A%multi == 0) && (B%multi == 0)) {
                product *= multi;
                A = A / multi;
                B = B / multi;
            } else {
                multi++;
            }
        }

        printf("%d\n", product*A*B);
    }

    return 0; 
}