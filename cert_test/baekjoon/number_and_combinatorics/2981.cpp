#include <stdio.h>

int arr[101] = {0,};

void print_arr(int *table, int count)
{
    for(int i =0 ; i < count; i++) {
        printf("%d ", table[i]);
    }
    printf("\n");
}

void merge(int *num, int begin, int mid, int end)
{
    int i = begin;
    int j = mid+1;
    int k = 0;
    int size = end - begin + 1;
    int *sorted = new int[size];

    while(i <= mid && j <= end) {
        if(num[i] <= num[j]) {
            sorted[k++] = num[i++];
        } else {
            sorted[k++] = num[j++];
        }
    }

    if( i > mid) {
        for(int t = j ; t <= end ; t++) {
            sorted[k++] = num[t];
        }
    } else {
        for(int t = i ; t <= mid ; t++) {
            sorted[k++] = num[t];
        }
    }

    for(int t = 0; t < size; t++) {
        num[begin+t] = sorted[t]; 
    }

    delete[] sorted;
}

void merge_sort(int *num, int begin, int end)
{
    if(begin < end) {
        int mid = (begin + end) / 2;
        merge_sort(num, begin, mid);
        merge_sort(num, mid + 1, end);
        merge(num, begin, mid, end);
    }
}

int get_gcd(int a, int b)
{
    return (a % b == 0) ? b : get_gcd(b, a%b);
}

int main(void)
{
    int n;
    scanf("%d", &n);

    for(int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }

    merge_sort(arr, 0, n -1);
    // print_arr(arr, n);
    
    int gcd = arr[1] - arr[0];
    for(int i = 2; i < n; i++) {
        gcd = get_gcd(gcd, arr[i] - arr[i-1]);
    }

    int sol[500];
    int idx = 0;
    for(int i = 2; i*i <= gcd; i++) {
        if(gcd % i == 0) {
            sol[idx++] = i;
            if(i != gcd / i) sol[idx++] = gcd / i;
        }
    }
    sol[idx++] = gcd;
    // printf("idx : %d\n", idx);
    merge_sort(sol, 0, idx - 1);
    print_arr(sol, idx);

    return 0;
}