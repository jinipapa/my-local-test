#include <stdio.h>

void merge(int* table, int start, int mid, int end)
{
    int i = start;
    int j = mid + 1;
    int k = 0;
    int size = end - start + 1;
    int *sorted = new int[size];

    while(i <= mid && j <= end) {
        if(table[i] <= table[j]) {
            sorted[k++] = table[i++];
        } else {
            sorted[k++] = table[j++];
        }
    }

    if(i > mid) {
        for(unsigned int t = j; t <= end ; t++) {
            sorted[k++] = table[t];
        }
    } else {
        for(unsigned int t = i; t <= mid ; t++) {
            sorted[k++] = table[t];
        }
    }

    for(unsigned int t = 0; t < size; t++) {
        table[start+t] = sorted[t];
    }
    delete[] sorted;
}

void merge_sort(int* table, int start, int end)
{
    if(start < end) {
        int mid = (start + end) / 2;
        merge_sort(table, start, mid);
        merge_sort(table, mid+1, end);
        merge(table, start, mid, end);
    }
}

int main(void)
{
    unsigned int N;
    scanf("%d", &N);

    int* table = new int[N];
    for(unsigned int i = 0; i < N; i++) {
        scanf("%d", &table[i]);
    }

    merge_sort(table, 0, N -1);

    for(unsigned int i = 0; i < N;i++) {
        printf("%d\n", table[i]);
    }

    delete[] table;
    return 0;
}