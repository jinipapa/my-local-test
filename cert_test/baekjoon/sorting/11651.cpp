#include <stdio.h>

class Point {
public:
    Point() = default;
    ~Point() = default;
    
    void SetPoint(int x, int y) {
        x_ = x;
        y_ = y;
    }

    int GetX() 
    {
        return x_;
    }
    int GetY()
    {
        return y_;
    }
    Point& operator=(const Point& p) {
        x_ = p.x_;
        y_ = p.y_;
        return *this;
    }

    friend bool operator < (const Point& left, Point& right) {
        if(left.y_ > right.y_) return false;
        if(left.y_ < right.y_) return true;
        if(left.y_ == right.y_) {
            if(left.x_ < right.x_) return true;
        }
        return false;
    }

private:
    int x_;
    int y_;
};


void merge_point(Point* data, int start, int mid, int end)
{
    int i = start;
    int j = mid + 1;
    int k = 0;
    int size = end - start + 1;

    Point* sorted = new Point[size];

    while(i <= mid && j <= end) {
        if(data[i] < data[j]) sorted[k++] = data[i++];
        else sorted[k++] = data[j++];
    }

    if( mid < i) {
        for(unsigned int t = j ; t <= mid; t++) {
            sorted[k++] = data[t];
        }
    } else {
        for(unsigned int t = i ; t <= end; t++) {
            sorted[k++] = data[t];
        }
    }

    for(unsigned int t = 0; t < size; t++) {
        data[start + t] = sorted[t];
    }

    delete[] sorted;
}

void point_sort(Point* data, int start, int end)
{
    if( start < end) {
        int mid = (start + end) / 2;
        point_sort(data, start, mid);
        point_sort(data, mid+1, end);
        merge_point(data, start, mid, end);
    }
}

int main(void)
{
    int N;
    scanf("%d", &N);
    Point *table = new Point[N];
    for(unsigned int i = 0; i < N; i++)
    {
        int x, y;
        scanf("%d %d", &x, &y);
        table[i].SetPoint(x, y);
    }

    point_sort(table, 0, N -1);

    for(unsigned int i = 0 ; i < N; i++) {
        printf("%d %d\n", table[i].GetX(), table[i].GetY());
    }


    delete[] table;

    return 0;
}
