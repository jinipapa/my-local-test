#include <stdio.h>

bool has_num_666(int num)
{
    int count = 0;
    while(num != 0) {
        if(num % 10 == 6) {
            count++;
        } else {
            count = 0;
        }
        num = num / 10;
        if(count == 3) return true;
    }

    return false;
}

int main(void)
{
    int N;
    scanf("%d", &N);

    int count = 0;
    int num = 665;
    while(count != N) {
        num++;
        if(has_num_666(num) == false) continue;
        // printf("%d ", num);
        count++;
    }

    printf("%d\n", num);

    return 0;
}