#include <stdio.h>

class Line {
public:
    Line(int left, int right) {
        left_ = left;
        right_ = right;
    }
    Line() = default;
    ~Line() = default;

    int GetLeft(){return left_;}
    int GetRight(){return right_;}

    Line& operator = (const Line& ref) {
        left_ = ref.left_;
        right_ = ref.right_;
        return *this;
    }

    friend bool operator >= (const Line& c1, const Line& c2) {
        return c1.left_ >= c2.left_;
    }

    friend bool operator <= (const Line& c1, const Line& c2) {
        return c1.left_ <= c2.left_;
    }

private:

    int left_;
    int right_;
};

Line line[101];

void merge(Line* line, int begin, int middle, int end)
{
    int i = begin;
    int j = middle+1;
    int k = 0;
    int size = end - begin + 1;
    Line* sorted = new Line[size];

    while(i <= middle && j <= end) {
        if(line[i] <= line[j]) {
            sorted[k++] = line[i++];
        } else {
            sorted[k++] = line[j++];
        }
    }

    if(i > middle) {
        for(int t = j ; t <= end; t++) {
            sorted[k++] = line[t];
        }
    } else {
        for(int t = i ; t <= middle; t++) {
            sorted[k++] = line[t];
        }
    }

    for(int t = 0; t < size; t++) {
        line[begin + t] = sorted[t];
    }
    
    delete[] sorted;
}

int Max(int a, int b)
{
    if(a > b) return a;
    return b;
}

void merge_sort(Line* line, int begin, int end) 
{
    if(begin < end) {
        int middle = (begin + end) / 2;
        merge_sort(line, begin, middle);
        merge_sort(line, middle+1, end);
        merge(line, begin, middle, end);
    }
}

int main(void)
{
    int n;
    scanf("%d", &n);

    for(int i = 0; i < n; i++) {
        int left, right;
        scanf("%d %d", &left, &right);
        Line temp(left, right);
        line[i] = temp;
    }

    merge_sort(line, 0, n - 1);

    // printf("======================================\n");
    // printf("======================================\n");
    // printf("======================================\n");

    // for(int i = 0; i < n; i++) {
    //     printf("%d %d\n", line[i].GetLeft(), line[i].GetRight());
    // }


    int DP[n];
    DP[0] = 1;

    int max = 1;
    for(int i = 1; i < n; i++) {
        DP[i] = 1;
        for(int j = i - 1; j >= 0; j--) {
            if(line[i].GetRight() > line[j].GetRight()) {
                DP[i] = Max(DP[i], DP[j] + 1);
            }
        }

        max = Max(max, DP[i]);
    }

    // for(int i = 0; i < n; i++) {
    //     printf("%d ", DP[i]);
    // }
    // printf("\n");

    printf("%d\n", n - max);

    return 0;
}
