#include <stdio.h>

int Max(int a, int b)
{
    if(a > b) return a;
    return b;
}

int main(void)
{
    int n; 
    scanf("%d", &n);
    int value[100001];
    for(int i = 0; i < n; i++) {
        scanf("%d", &value[i]);
    }

    int DP[100001];
    DP[0] = value[0];
    for(int i = 0; i < n ; i++) {
        DP[i] = Max(DP[i-1] + value[i], value[i]);
    }

    int max = DP[0];
    for(int i = 1; i< n; i++) {
        if(max < DP[i]) max = DP[i];
    }

    printf("%d\n", max);

    return 0;
}