#include <stdio.h> 


int w[21][21][21];


int calc(int a, int b, int c)
{
    if(a <= 0 || b <= 0 || c <= 0)
    {
        return 1;
    }
}

int main(void)
{
    for(int i = 0; i < 21; i++) {
        for(int j = 0; j < 21; j++) {
            for(int k = 0; k < 21; k++) {
                w[i][j][k] = 0;
            }
        }
    }

    while(1) 
    {
        int a, b, c;
        scanf("%d %d %d", &a, &b, &c);
        if(a == -1 && b == -1 && c == -1) {
            break;
        }

        int res=calc(a, b, c);
        printf("w(%d, %d, %d) = %d\n", a, b, c, res);
    }
    return 0;
}