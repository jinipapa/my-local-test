#include <stdio.h>
#include <string.h>

int get_length(char word[])
{
    if(word[0] == 'c') {
        if(word[1] == '=' || word[1] == '-') return 2;
    } else if(word[0] == 'd') {
        if(word[1] == 'z' && word[2] == '=') return 3;
        if(word[1] == '-') return 2;
    } else if(word[0] == 'l' && word[1] == 'j') return 2;
    else if(word[0] == 'n' && word[1] == 'j') return 2;
    else if(word[0] == 's' && word[1] == '=') return 2;
    else if(word[0] == 'z' && word[1] == '=') return 2;

    return 1;
}

int main(void)
{
    char word[103] = {0,};
    scanf("%s", word);
    int idx = 0;
    int count = 0;
    while (idx < strlen(word)) {
        idx  += get_length(&word[idx]);
        // printf("%d\n", idx);
        count++;
    }

    printf("%d\n", count);

    return 0;
}